import { System } from "./System.mjs"
import { Vector } from "./Vector.mjs"
import { PID } from "./PID.mjs"

const svgns = "http://www.w3.org/2000/svg"
const svg = document.getElementsByTagName("svg")[0]

const WIDTH = window.innerWidth * 0.98
const HEIGHT = window.innerHeight * 0.8

const config = {
    targetCount: 50,
    systemSize: 10,
    systemColor: "blue",
    targetSize: 10,
    notValidatedTargetColor: "red",
    nextTargetColor: "orange",
    validatedTargetColor: "green",
    xForce: -0.05, // Wind
    yForce: 0.3721 // Gravity of Mars / 10
}

svg.setAttribute("width", WIDTH)
svg.setAttribute("height", HEIGHT)

function clear() {
    while (svg.lastChild) {
        svg.removeChild(svg.lastChild);
    }
}

function circle(cx, cy, r, color) {
    var circle = document.createElementNS(svgns, 'circle');
    circle.setAttribute('cx', cx);
    circle.setAttribute('cy', cy);
    circle.setAttribute('r', r);
    circle.setAttribute('style', `fill: ${color};`);
    svg.appendChild(circle);
}

const system = new System(50, 50)

const targets = Array.from(Array(config.targetCount)).map(() =>
    new Vector(
        Math.floor(Math.random() * WIDTH),
        Math.floor(Math.random() * HEIGHT)
    )
)
let currentTargetIndex = 0

const xPID = new PID(0.002, 0.06, 1.4, true, [-1, 1], () => system.position.x, () => targets[currentTargetIndex].x)
const yPID = new PID(0.01, 0.425, 4, true, [-1, 1], () => system.position.y, () => targets[currentTargetIndex].y)

let lastTime = Date.now()
const intervalId = setInterval(() => {
    const distance = Vector.sub(targets[currentTargetIndex], system.position).mag()
    if (currentTargetIndex < targets.length - 1 && distance <= 5) {
        currentTargetIndex++
    }
    const now = Date.now()
    const dt = now - lastTime
    lastTime = now

    system.speed.x += xPID.update(dt)
    system.speed.y += yPID.update(dt)

    for (let force of [
        new Vector(config.xForce, 0),
        new Vector(0, config.yForce)
    ]) {
        system.speed.add(force)
    }

    system.update()

    clear()
    circle(system.position.x, system.position.y, config.systemSize, config.systemColor)
    targets.forEach((target, i) => {
        let color
        if (currentTargetIndex == i)
            color = config.nextTargetColor
        else if (i > currentTargetIndex)
            color = config.notValidatedTargetColor
        else
            color = config.validatedTargetColor
        circle(target.x, target.y, config.targetSize, color)
    })
}, 20)


// Control Panel

document.getElementById("revealPanel").addEventListener("click", () => {
    const panel = document.getElementById("controlPanel")
    if (panel.classList.contains("hidden"))
        panel.classList.remove("hidden")
    else
        panel.classList.add("hidden")
})


function bindElement(elementId, setter = () => { }, getter = () => 0) {
    const element = document.getElementById(elementId)
    element.value = getter()
    element.addEventListener("change", (event) => {
        setter(event.target.value)
    })

}

[
    { id: "xPIDPG", setter: (newValue) => xPID.proportionalGain = newValue, getter: () => xPID.proportionalGain },
    { id: "yPIDPG", setter: (newValue) => yPID.proportionalGain = newValue, getter: () => yPID.proportionalGain },

    { id: "xPIDIG", setter: (newValue) => xPID.integralGain = newValue, getter: () => xPID.integralGain },
    { id: "yPIDIG", setter: (newValue) => yPID.integralGain = newValue, getter: () => yPID.integralGain },

    { id: "xPIDDG", setter: (newValue) => xPID.derivativeGain = newValue, getter: () => xPID.derivativeGain },
    { id: "yPIDDG", setter: (newValue) => yPID.derivativeGain = newValue, getter: () => yPID.derivativeGain },

    { id: "xPIDUS", setter: (newValue) => xPID.integralSaturation[1] = newValue, getter: () => xPID.integralSaturation[1] },
    { id: "yPIDUS", setter: (newValue) => yPID.integralSaturation[1] = newValue, getter: () => yPID.integralSaturation[1] },
    { id: "xPIDLS", setter: (newValue) => xPID.integralSaturation[0] = newValue, getter: () => xPID.integralSaturation[0] },
    { id: "yPIDLS", setter: (newValue) => yPID.integralSaturation[0] = newValue, getter: () => yPID.integralSaturation[0] },

    { id: "systemSize", setter: (newValue) => config.systemSize = newValue, getter: () => config.systemSize },
    { id: "systemColor", setter: (newValue) => config.systemColor = newValue, getter: () => config.systemColor },
    { id: "targetSize", setter: (newValue) => config.targetSize = newValue, getter: () => config.targetSize },
    { id: "nvTarget", setter: (newValue) => config.notValidatedTargetColor = newValue, getter: () => config.notValidatedTargetColor },
    { id: "nTarget", setter: (newValue) => config.nextTargetColor = newValue, getter: () => config.nextTargetColor },
    { id: "vTarget", setter: (newValue) => config.validatedTargetColor = newValue, getter: () => config.validatedTargetColor },
    { id: "xForce", setter: (newValue) => config.xForce = parseFloat(newValue), getter: () => config.xForce },
    { id: "yForce", setter: (newValue) => config.yForce = parseFloat(newValue), getter: () => config.yForce },

].forEach(options => { bindElement(options.id, options.setter, options.getter) })

document.getElementById("moveTargets").addEventListener("click", () => {
    targets.forEach(target => {
        target.x = Math.floor(Math.random() * WIDTH)
        target.y = Math.floor(Math.random() * HEIGHT)
    })
})

document.getElementById("resetTargets").addEventListener("click", () => {
    currentTargetIndex = 0
})
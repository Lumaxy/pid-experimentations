import { Vector } from "./Vector.mjs"

export class System {
    constructor(x, y) {
        this.position = new Vector(x, y)
        this.speed = new Vector(0.01, 0)
    }

    update() {
        this.position.x += this.speed.x
        this.position.y += this.speed.y
    }
}
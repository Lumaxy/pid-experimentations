export class PID {
    constructor(proportionalGain, integralGain, derivativeGain, useValueDerivative, integralSaturation, getCurrentValue = () => 0, getTargetValue = () => 0) {
        this.proportionalGain = proportionalGain
        this.integralGain = integralGain
        this.derivativeGain = derivativeGain

        this.getCurrentValue = getCurrentValue
        this.getTargetValue = getTargetValue

        this.isDerivativeInitialized = false
        this.useValueDerivative = useValueDerivative

        this.lastError = 0
        this.lastValue = this.getCurrentValue()

        this.integrationStored = 0
        this.integralSaturation = integralSaturation
    }

    update(dt) {
        const currentValue = this.getCurrentValue()
        const targetValue = this.getTargetValue()
        const error = targetValue - currentValue

        const P = error * this.proportionalGain

        const errorRateOfChange = (error - this.lastError) / dt
        const valueRateOfChange = (currentValue - this.lastValue) / dt

        const deriveMeasure = this.useValueDerivative ? -valueRateOfChange : errorRateOfChange
        const D = this.isDerivativeInitialized ? (deriveMeasure * this.derivativeGain) : 0
        this.isDerivativeInitialized = true

        this.integrationStored += error * dt

        if (this.integrationStored > this.integralSaturation[1]) {
            this.integrationStored = this.integralSaturation[1]
        } else if (this.integrationStored < this.integralSaturation[0]) {
            this.integrationStored = this.integralSaturation[0]
        }

        const I = this.integrationStored * this.integralGain

        this.lastError = error
        this.lastValue = currentValue

        return P + I + D
    }
}
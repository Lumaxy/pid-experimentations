export class Vector {
    constructor(x, y) {
        this.x = x
        this.y = y
    }

    static sub(a, b) {
        return new Vector(a.x - b.x, a.y - b.y)
    }

    clone() {
        return new Vector(this.x, this.y)
    }

    mag() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
    }

    norm() {
        const mag = this.mag()
        if (mag != 0) {
            this.x /= mag
            this.y /= mag
        }
    }

    mult(factor) {
        this.x *= factor
        this.y *= factor
    }

    add(a) {
        this.x += a.x
        this.y += a.y
    }

    sub(a) {
        this.x -= a.x
        this.y -= a.y
    }
    rotateAnticlockwise(angleInDegree) {
        const angleInRadian = angleInDegree * Math.PI / 180
        const tmp_x = Math.cos(angleInRadian) * this.x - Math.sin(angleInRadian) * this.y
        const tmp_y = Math.sin(angleInRadian) * this.x + Math.cos(angleInRadian) * this.y
        this.x = tmp_x
        this.y = tmp_y
    }
}